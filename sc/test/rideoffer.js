const RideOffer = artifacts.require("RideOffer");

contract("RideOffer", accounts => {
  const driver = accounts[0];
  const first_passanger = accounts[1];
  const second_passenger = accounts[2];
  const third_passenger = accounts[3];

  it("should initialize intermediate payment checkpoints and join the ride as a driver", () => {
    let rideOffer;
    let advancePayment;
    let freeSpots;
    return RideOffer.deployed()
      .then(instance => {
        rideOffer = instance;
        return instance.initCheckpoints([1, 2], [3, 4]);
      })
      .then(resp => rideOffer.freeSpots())
      .then(resp => {
        freeSpots = resp.toNumber();
        return rideOffer.advancePayment();
      }).then(resp => {
        advancePayment = resp.toNumber();
        return rideOffer.joinRide.sendTransaction(0, 0, { from: driver, value: advancePayment });
      })
      .then(resp => rideOffer.freeSpots())
      .then(resp => assert.equal(resp.toNumber(), freeSpots - 1, "Number of spots should be decreased by 1 because the driver joined the ride!"))
  });

  it("leave ride should fail because the passenger didn't join the ride!", async () => {
    let rideOffer = await RideOffer.deployed();
    let err = null;
    try {
      await rideOffer.leaveRide({from: second_passenger});
    } catch (error) {
      // console.log(error);
      err = error;
    }
    assert.ok(err instanceof Error);
  });

  it("three passengers should join the ride using valid advance payment", () => {
    let rideOffer;
    let advancePayment;
    return RideOffer.deployed()
      .then(instance => {
        rideOffer = instance;
        return rideOffer.advancePayment();
      })
      .then(resp => {
        advancePayment = resp.toNumber();
        return rideOffer.joinRide.sendTransaction(0, 0, { from: first_passanger, value: advancePayment });
      })
      .then(resp => rideOffer.joinRide.sendTransaction(0, 0, { from: second_passenger, value: advancePayment }))
      .then(resp => rideOffer.joinRide.sendTransaction(0, 0, { from: third_passenger, value: advancePayment }))
      .then(resp => rideOffer.freeSpots())
      .then(resp => {
        assert.equal(
          resp.toNumber(),
          0,
          "There should be no spot left!"
        );
      })
  });

  it("passenger three tries to rejoin already joined ride", async () => {
    let rideOffer = await RideOffer.deployed();
    var resp = await rideOffer.advancePayment();
    let advancePayment = resp.toNumber();
    let err = null;
    try {
      resp = await rideOffer.joinRide.sendTransaction(0, 0, { from: second_passenger, value: advancePayment });
    } catch (error) {
      // console.log(error)
      err = error;
    }
    assert.ok(err instanceof Error);
  });

  it("third passenger should leave and rejoin the ride", () => {
    let rideOffer;
    let advancePayment;
    let freeSpots;
    return RideOffer.deployed()
      .then(instance => {
        rideOffer = instance;
        return rideOffer.advancePayment();
      })
      .then(resp => {
        advancePayment = resp.toNumber();
        return rideOffer.freeSpots();
      })
      .then(resp => {
        freeSpots = resp.toNumber();
        return rideOffer.leaveRide({ from: third_passenger });
      })
      .then(resp => rideOffer.freeSpots())
      .then(resp => {
        assert.equal(resp.toNumber(), (freeSpots + 1), "Free spots should increase by 1 after leaving ride!");
        return rideOffer.joinRide.sendTransaction(0, 0, { from: third_passenger, value: advancePayment });
      })
      .then(resp => rideOffer.freeSpots())
      .then(resp => {
        assert.equal(resp.toNumber(), freeSpots, "Free spots should decrease by 1 after joining ride!");
      })
  });

  it("third passenger should get his advance payment back when leaving the ride in initiating state", async () => {
    let rideOffer = await RideOffer.deployed();
    let advancePayment = (await rideOffer.advancePayment()).toNumber();
    let depositBefore = (await rideOffer.getDepositsOf(third_passenger)).toNumber();
    let txResult = await rideOffer.leaveRide({from: third_passenger});  
    let depositAfter = (await rideOffer.getDepositsOf(third_passenger)).toNumber();
    assert(
      depositBefore >= advancePayment && depositAfter == 0,
      "Deposit after leaving the ride should be zero!"
    );
  });

   
  it("third passenger should join the ride using valid advance payment", async() => {
    let rideOffer = await RideOffer.deployed();
    let advancePayment = (await rideOffer.advancePayment()).toNumber();
    await rideOffer.joinRide.sendTransaction(0, 0, { from: third_passenger, value: advancePayment })
  });


  it("start ride should fail because it is called too early", async () => {
    let rideOffer = await RideOffer.deployed();
    let err = null;
    try {
      await rideOffer.startRide();
    } catch (error) {
      // console.log(error);
      err = error;
    }
    assert.ok(err instanceof Error);
  });

  it("start ride should succeed because it is called after estDepartureTime", async () => {
    let rideOffer = await RideOffer.deployed();
    console.log("Starting ride in 5 seconds...")
    await timeout(5000);
    await rideOffer.startRide(); // this should work now!
  });

  // make payments with all passenger accounts (account_two, account_three an account_four)
  it("all three passenger accounts should make payments to the driver", async () => {
    let rideOffer = await RideOffer.deployed();
    let cost = await rideOffer.calculateCostBetween(0, 0).then(cost => cost.toNumber()); // since there is only one checkpoint, start and end checkpoint index is the same (0)
    let balanceBeforePayments = await web3.eth.getBalance(driver);
    await rideOffer.makePayment.sendTransaction(0, { from: first_passanger, value: cost })
    await rideOffer.makePayment.sendTransaction(0, { from: second_passenger, value: cost })
    await rideOffer.makePayment.sendTransaction(0, { from: third_passenger, value: cost })
    let balanceAfterPayments = await web3.eth.getBalance(driver);

    assert.equal(balanceBeforePayments,
      balanceAfterPayments - cost * 3,
      "Driver's account balance should increase by cost*3!");
  });

  it("stop ride should fail because it is called too early", async () => {
    let rideOffer = await RideOffer.deployed();
    let err = null;
    try {
      await rideOffer.startRide();
    } catch (error) {
      // console.log(error);
      err = error;
    }
    assert.ok(err instanceof Error);
  });

  it("user should open and close a dispute", async() => {
    let rideOffer = await RideOffer.deployed();
    let numberOfDisputesBeforeOpen = (await rideOffer.getNumberOfDisputes()).toNumber();
    let openDisputeTxResp = await rideOffer.openDispute("The driver didn't to the pickup location in time.", {from: accounts[1]});
    let numberOfDisputesAfterOpen = (await rideOffer.getNumberOfDisputes()).toNumber();
    await rideOffer.closeDispute({from: accounts[1]});
    let numberOfDisputesAfterClose = (await rideOffer.getNumberOfDisputes()).toNumber();

    assert.equal(
      numberOfDisputesBeforeOpen,
      numberOfDisputesAfterOpen-1,
      "Number of disputes should increase by 1!"
    )

    assert.equal(
      numberOfDisputesAfterClose,
      numberOfDisputesBeforeOpen,
      "Number of disputes after close should be equal to number of disputes before open!"
    )
  });

  it("stop ride should succeed because it is called after estArrivalTime", async () => {
    let rideOffer = await RideOffer.deployed();
    console.log("Stopping ride in 5 seconds...")
    await timeout(5000); // combined with previous 5 seconds delay should yeld 10+ seconds (which is more than estArrivalTime)
    await rideOffer.stopRide();
  });

  it("ride offer should be in finished state", () => {
    return RideOffer.deployed()
      .then(instance => instance.state())
      .then(state => assert.equal(state.toNumber(), 2, "Ride offer is not in finished (2) state!"));
  });

  function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

});
