# Useful commands 

## Init:
migrate

let accounts = await web3.eth.getAccounts()

let rideOffer = await RideOffer.deployed()

<!-- Driver needs to initialize payment checkpoints before starting the ride: -->
rideOffer.initCheckpoints()

## Get advance payment
let advancePayment = (await rideOffer.advancePayment()).toNumber()

## Join ride:
rideOffer.joinRide.sendTransaction(0, 0, {from: accounts[0], value: advancePayment})

rideOffer.joinRide.sendTransaction(0, 0, {from: accounts[1], value: advancePayment})

rideOffer.joinRide.sendTransaction(0, 0, {from: accounts[2], value: advancePayment})

rideOffer.joinRide.sendTransaction(0, 0, {from: accounts[3], value: advancePayment})

## Free spots:
rideOffer.freeSpots()

## Leave ride:
rideOffer.leaveRide({from: accounts[1]})

## Start the ride:
rideOffer.startRide()

## Get current driver location:
let currentCheckpointIndex = await rideOffer.currentCheckpointIndex().then(index => index.toNumber())

## Get cost for getting from A to B:
let totalCost = (await rideOffer.calculateCostBetween(0, 0)).toNumber()

## Making payments on the go (during active state/driving)

await rideOffer.makePayment.sendTransaction(0, {from: accounts[2], value: totalCost}) 

**(TODO - RideOffer's sc need to handle last payment index to take into account advance payment or enable refunding at the end if there were no disputes)**

## Get account balance:
<!-- driver's account balance -->
web3.eth.getBalance(accounts[0])

## Open a dispute
<!-- it does not work yet -->
await rideOffer.openDispute("The driver didn't to the pickup location in time.", {from: accounts[2]}) 

let numberOfDisputes = (await rideOffer.getNumberOfDisputes()).toNumber()

## Close a dispute

await rideOffer.closeDispute({from: accounts[2]})

## Creating wrappers for Java 

[web3j - java smart contract wrappers](https://github.com/web3j/web3j#working-with-smart-contracts-with-java-smart-contract-wrappers)

web3j truffle generate build/contracts/RideOffer.json -o ./build -p org.tomicdev.hashdrive.examples

