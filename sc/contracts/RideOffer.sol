pragma solidity ^0.5.0;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "./HashdriveRefundEscrow.sol";

contract RideOffer is Ownable {

    State public state;
    VehicleType public vehicleType;
    uint40 public priceTinybars; // there are 100,000,000 tinybars in one hbar, max value in hbars (40 bits): 10995
    uint8 public freeSpots;
    uint public estDepartureTime;
    uint public estArrivalTime;
    string public lineStringFileId; // e.g. "0,0,1234"

    uint8 public timeLimitStart = 0;     // in minutes
    uint8 public distanceLimitStart = 0; // in meters
    uint8 public timeLimitEnd = 0;       // in minutes
    uint8 public distanceLimitEnd = 0;   // in meters

    uint public advancePayment;
    HashdriveRefundEscrow private re;

    uint32[] public checkPoints; // checkpoints for making intermediate payments (defined by the driver) node ids
    mapping(uint32 => uint8) private ridePercentageMap; // lookup map for calculating intermediate payments
    uint8 public currentCheckpointIndex = 0;

    Participant[] private participantsArray;
    mapping(address => Participant) private participantsMap;

    address[] public disputers;
    mapping(address => string) public openDisputes;

    constructor(VehicleType _vehicleType, uint40 _priceTinybars, uint8 _freeSpots,
      uint _estDepartureTime, uint _estArrivalTime, string memory _lineStringFileId) public {
        re = new HashdriveRefundEscrow(msg.sender);
        state = State.Initiating;
        setVehicleType(_vehicleType);
        setPriceTinybars(_priceTinybars);
        setFreeSpots(_freeSpots);
        setTime(_estDepartureTime, _estArrivalTime);
        setPath(_lineStringFileId);
        re.enableRefunds(); // refunds enabled while in initiating stage
    }

    function setVehicleType(VehicleType _vehicleType) internal {
        vehicleType = _vehicleType;
    }

    function setPriceTinybars(uint40 _priceTinybars) internal {
        priceTinybars = _priceTinybars;
        advancePayment = priceTinybars/10; // 10% of priceTinybars
    }

    function setFreeSpots(uint8 _freeSpots) internal {
        freeSpots = _freeSpots;
    }

    function setTime(uint _estDepartureTime, uint _estArrivalTime) internal {
        require(_estDepartureTime > now, "Departure time should be in the future!");
        require(_estArrivalTime > _estDepartureTime, "Arrival time should be greater than departure time!");
        estDepartureTime = _estDepartureTime;
        estArrivalTime = _estArrivalTime;
    }

    function setPath(string memory _lineStringFileId) internal {
        lineStringFileId = _lineStringFileId;
    }

    function setLimits(uint8 _timeLimitStart, uint8 _distanceLimitStart, uint8 _timeLimitEnd, uint8 _distanceLimitEnd) public onlyOwner {
        timeLimitStart = _timeLimitStart;
        distanceLimitStart = _distanceLimitStart;
        timeLimitEnd = _timeLimitEnd;
        distanceLimitEnd = _distanceLimitEnd;
    }

    function initCheckpoints(uint32[] memory _checkPoints, uint8[] memory _checkPointPercentages) public onlyOwner {
        require(checkPoints.length == 0, "Checkpoints already initialized!");
        require(_checkPoints.length > 0, "Checkpoints should not be empty!");
        require(_checkPoints.length == _checkPointPercentages.length, "Checkpoints and checkpoint percentages should be the same size!");

        for (uint i = 0; i < _checkPoints.length; i++) {
            addCheckpoint(_checkPoints[i], _checkPointPercentages[i]);
        }
    }

     function addCheckpoint(uint32 checkPoint, uint8 ridePercentage) public onlyOwner {
        require(ridePercentageMap[checkPoint] == 0, "Checkpoint already exists!");
        require(ridePercentage > 0 && ridePercentage <= 100, "Ride percentage is not in [1, 99]!");
        checkPoints.push(checkPoint);
        ridePercentageMap[checkPoint] = ridePercentage;
    }

    function makePayment(uint8 checkpointIndex) public payable {
        require(state == State.Active, "Payment can be made only while ride offer is in active state!");
        require(participantsMap[msg.sender].isActive, "Only active participants can make a payment!");
        require(msg.sender != owner(), "The driver should not make payments to himself!");
        require(checkpointIndex >= participantsMap[msg.sender].fromCheckpointIndex &&
            checkpointIndex <= participantsMap[msg.sender].toCheckpointIndex,
            "checkpointIndex is not in [fromCheckpointIndex, toCheckpointIndex]!");
        require(!participantsMap[msg.sender].madePayments[checkpointIndex], "You already payed for checkpointIndex!");
        address payable driverAddress = address(uint160(owner()));
        driverAddress.transfer(msg.value); // send funds to the driver
        participantsMap[msg.sender].madePayments[checkpointIndex] = true;
    }

    function startRide() public onlyOwner {
        require(state == State.Initiating, "You are not in initiating state!");
        require(checkPoints.length > 0, "You didn't initialize intermediate payment checkpoints (use one of initCheckpoints method overloads)!");
        require(now >= estDepartureTime, "You can not start the ride before estDepartureTime!");
        require(participantsMap[msg.sender].isActive, "You need to join the ride before starting it!");
        state = State.Active;
        re.close(); // the passengers (and the driver) can no longer withdraw advance payment when canceling the ride
    }

    function stopRide() public onlyOwner {
        require(state != State.Finished, "Already in finished state!");
        require(now >= estArrivalTime, "You can not stop the ride before estArrivalTime!");
        // TODO - require that no complaint exists (e.g.: passsanger did not get picked up, could only be made while in Active state)
        state = State.Finished;
        if(disputers.length == 0) {
            re.beneficiaryWithdraw();
        } // else handleDisputes by the hashdrive team (at first)
    }

    function joinRide(uint8 fromCheckpointIndex, uint8 toCheckpointIndex) public payable {
        require(msg.value >= advancePayment, "Advance payment should be at least 10% of the priceTinybars!");
        require(freeSpots > 0, "No free spots left!");
        require(participantsArray.length > 0 || msg.sender == owner(), "Wait for the driver to join the ride!");
        addParticipant(fromCheckpointIndex, toCheckpointIndex);
    }

    function addParticipant(uint8 fromCheckpointIndex, uint8 toCheckpointIndex) internal {
        require(checkPoints.length > 0, "The driver didn't initialize intermediate payment checkpoints!");
        require(fromCheckpointIndex >= 0 && fromCheckpointIndex < checkPoints.length,
            "fromCheckpointIndex is not in [0, checkPoints.length-1]!");
        require(toCheckpointIndex >= fromCheckpointIndex && toCheckpointIndex < checkPoints.length,
            "toCheckpointIndex is not in [fromCheckpointIndex, checkPoints.length-1]!");
        require(!participantsMap[msg.sender].isActive, "Passenger already joined the ride!");
        Participant memory participant;
        uint40 totalCost = calculateCostBetween(fromCheckpointIndex, toCheckpointIndex);
        if(participantsArray.length == 0 || participantsMap[msg.sender].index == 0) {
            participant = Participant(msg.sender, fromCheckpointIndex, toCheckpointIndex, totalCost, true, uint8(participantsArray.length));
            participantsArray.push(participant);
            participantsMap[msg.sender] = participant;
        } else {
            participantsMap[msg.sender].fromCheckpointIndex = fromCheckpointIndex;
            participantsMap[msg.sender].toCheckpointIndex = toCheckpointIndex;
            participantsMap[msg.sender].totalCost = totalCost;
            participantsMap[msg.sender].isActive = true;
        }
        re.deposit.value(msg.value)(msg.sender);
        freeSpots--;
    }

    function calculateCostBetween(uint8 fromCheckpointIndex, uint8 toCheckpointIndex) public view returns(uint40) {
        uint32 fromGeoPoint = checkPoints[fromCheckpointIndex];
        uint40 fromPercentage = ridePercentageMap[fromGeoPoint];
        uint40 diff = fromPercentage;
        if(fromCheckpointIndex != toCheckpointIndex) {
            uint32 toGeoPoint = checkPoints[toCheckpointIndex];
            uint40 toPercentage = ridePercentageMap[toGeoPoint];
            diff = toPercentage - fromPercentage;
        }
        uint40 cost = priceTinybars * diff;
        return cost;
    }

    function leaveRide() public {
        require(msg.sender != owner(), "Driver can not leave the ride!");
        require(participantsMap[msg.sender].isActive, "Only active participants can leave ride!");
        require(state != State.Finished, "You can not leave ride in finished state!");
        if(state == State.Initiating) {
            re.withdraw(msg.sender); // release funds back to the passenger because he left the right in time.
        }
        participantsMap[msg.sender].isActive = false;
        freeSpots++;
    }

    function openDispute(string memory note) public {
        require(state == State.Active, "A dispute can be added only during active state!");
        require(participantsMap[msg.sender].isActive, "Only active participants can open a dispute!");
        require(bytes(note).length > 0, "Note should be at least 1 character long!");
        require(bytes(openDisputes[msg.sender]).length == 0, "User already opened a dispute!");
        openDisputes[msg.sender] = note;
        disputers.push(msg.sender);
    }

    function closeDispute() public {
        require(bytes(openDisputes[msg.sender]).length > 0, "User does not have an open dispute!");
        openDisputes[msg.sender] = '';
        uint index = disputers.length; // invalid index
        for(uint i = 0; i < disputers.length; i++) {
            if(disputers[i] == msg.sender) {
                index = i;
                break;
            }
        }
        require(index != disputers.length, "User did not open any dispute!");
        disputers[index] = disputers[disputers.length-1];
        disputers.length--;
        openDisputes[msg.sender] = '';
    }

    function getNumberOfDisputes() public view returns (uint) {
        return disputers.length;
    }

    function handleDisputes() public pure {
        // require(msg.sender == 0xb18bfb6d5da70fefd15b6ae70371c0b1e0ac55e4, "Currently complaints can be handled only by hashdrive team!");
        // if the driver violeted the agreement his advance payment will be collected by the hashdrive team as service fee,
        // otherwise advance payments will be properly distributed (if compliant was justified the affected passenger will get his advance payment back,
        // the remaining despoits will be send to the driver). This method should be part of extended RefundEscrow smart contract !!!
        require(false, "handleDisputes not yet implemented (should be called only by the hashdrive team at first)!");
    }

    //region development
    function deposit() public payable {
        re.deposit.value(msg.value)(msg.sender); // very imporant to pass value and sender to the payable function of another contract
    }

    function withdraw() public {
        re.withdraw(msg.sender);
    }

    function getDepositsOf(address payee) public view returns(uint256) {
        uint256 deposit1 = re.depositsOf(payee);
        return deposit1;
    }
    //endregion

    struct Participant {
        address addr;
        uint8 fromCheckpointIndex;
        uint8 toCheckpointIndex;
        uint40 totalCost;
        bool isActive;
        uint8 index;
        mapping(uint8 => bool) madePayments;
    }

    enum VehicleType {
        CAR, MOTORCYCLE, VAN, BOAT, JETSKI
    }

    enum State {
        Initiating, Active, Finished
    }
}