const Migrations = artifacts.require("Migrations");
const RideOffer = artifacts.require("RideOffer");

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  let estDepartureTime = new Date().getTime() + 5000; // in 5 seconds...
  let estArrivalTime = new Date().getTime() + 10000; //  in 10 seconds...
  console.log("estDepartureTime: " + estDepartureTime);
  console.log("estArrivalTime: " + estArrivalTime);
  let _vehicleType = 0, _priceTinybars = 100000, _freeSpots = 4, _timeLimitStart = 0, _distanceLimitStart = 0, _timeLimitEnd = 0, _distanceLimitEnd = 0;
  deployer.deploy(RideOffer, _vehicleType, _priceTinybars, _freeSpots, Math.floor(estDepartureTime/1000), Math.floor(estArrivalTime/1000), "lineStringFileIdStub");
  
};
